"""DRPCIV DB Handler -- Middleware between the endpoints and the DB for drpciv specific functionality & validation"""
from datetime import datetime, timezone as tz, date
import random
from loggez import loggez_logger as logger

from data_models import QuestionAnswers
from utils import (to_ms, SessionToken, render_sql_template, get_project_root,
                   format_list_for_query as fmt_list, parsed_str_type)
from db_handler import DBHandler

class UserNotExistsException(Exception): pass # pylint: disable=missing-class-docstring, multiple-statements

class DRPCIVDBHandler:
    """Wrapper on top of dbhandler for drpciv-specific methods. Rule: >1 line query must be rendered with jinja."""
    def __init__(self, db_handler: DBHandler):
        self.db_handler = db_handler
        self._check_and_make_tables()
        assert (res := self.query("select count(*) from qa").fetchall()[0][0]) == 1191, res # sanity check
        self.n_qs = 1191

    def query(self, *args, **kwargs):
        """wrapper on top of the underlying DB (sqlite/libsql) query method"""
        return self.db_handler.query(*args, **kwargs)

    # QuestionsAnswers stuff
    def get_questions(self, mode: str, n: int | None = None, ids: list[int] | None = None) \
            -> list[QuestionAnswers] | Exception:
        """returns a list of QuestionAnswers in modes: 'random' or 'fixed_ids' (outside logic i.e. active quiz)"""
        if mode not in (expected := ("random", "fixed_ids")):
            return Exception(f"Expected mode to be in {expected}, got '{mode}'")
        if mode == "random":
            if n is None or not isinstance(n, int) or n <= 0:
                return Exception(f"Mode 'random'. Expected 'n' to be a positive integer. Got {n}.")
            ids = random.sample(range(1, self.n_qs + 1), n)
        elif mode == "ids":
            if (ids is None or not isinstance(ids, list) or not all(isinstance(x, int) for x in ids)
                or len(set(ids)) != len(ids)):
                return Exception(f"Mode 'ids'. Expected 'ids' to be a list of unique ids. Got {ids}.")
        cols = ["id", "question", "answers", "correct_answers", "image"]
        quiz_data = self.query(f"select {fmt_list(cols)} from qa where id in ({fmt_list(ids)})").fetchall()
        assert (a := len(quiz_data)) == (b := len(ids)), f"returned rows: {a}. desired n ids: {b}. desired ids: {ids}"
        return [QuestionAnswers(qid=row[0], question=row[1], answers=row[2].split("|"),
                                correct_answers=list(map(int, row[3].split(","))),
                                image=(row[4].decode("ascii") if row[4] is not None else None))
                for row in quiz_data]

    # Quiz stuff
    def insert_new_quiz(self, quiz_id: str, user_id: int, date_start: str,
                        qa_ids: list[str], qa_correct_answers: list[list[int]]):
        """inserts a new quiz in the DB given a quiz id, user, date start and lis of questions"""
        qa_answers_str = "|".join([",".join(map(str, y)) for y in qa_correct_answers])
        query = render_sql_template(get_project_root() / "be/sql/insert_new_quiz.sql", quiz_id=quiz_id, user_id=user_id,
                                    date_start=date_start, qa_ids=fmt_list(qa_ids),
                                    correct_response_list=qa_answers_str)
        self.query(query, blocking=True)

    def get_active_quiz_by_user_name(self, user: str, max_duration_s: int) \
            -> tuple[str, date, list[int]] | None | Exception:
        """returns the active quiz of this user if it exists in the DB and it hasn't timed out"""
        if isinstance(user_id := self.get_id_from_user(user), Exception):
            return user_id
        query = render_sql_template(get_project_root() / "be/sql/get_active_quiz_by_user_name.sql", user_id=user_id)
        user_quiz_data = self.query(query).fetchall()
        assert len(user_quiz_data) in (0, 1), f"Must be 0 or 1, got {user_quiz_data}"
        if len(user_quiz_data) == 0:
            return None
        quiz_id, _quiz_date, _qa_ids = user_quiz_data[0]
        quiz_date, qa_ids = datetime.fromisoformat(_quiz_date), list(map(int, _qa_ids.split(",")))
        left_s = (datetime.now(tz.utc).replace(tzinfo=None) - quiz_date).total_seconds()
        if left_s > max_duration_s: # quiz expired
            return None
        return quiz_id, quiz_date, qa_ids

    def update_active_quiz(self, qid: str, user_answers: list[list[int]], date_end: str):
        """updates the quiz at the provided id with the user provided answers at the given date"""
        response_list = "|".join([",".join(map(str, ua)) for ua in user_answers]) # "[[1], [1,2]] -> "1|1,2"
        query = render_sql_template(get_project_root() / "be/sql/update_active_quiz.sql",
                                    response_list=response_list, date_end=date_end, id=qid)
        self.query(query, blocking=True)

    def get_quiz_by_id(self, qid: str) -> tuple:
        """returns the quiz given a quiz id."""
        return self.query(f"select * from questionnaires where id='{qid}'").fetchall()

    def get_correct_answers_from_quiz_id(self, qid: str) -> list[list[int]]:
        """returns the list of correct answers given a quiz id"""
        response_list = self.query(f"select correct_response_list from questionnaires where id='{qid}'").fetchall()
        assert len(response_list) == 1, response_list
        res = [[int(y) for y in x.split(",")] for x in response_list[0][0].split("|")]
        return res

    def get_all_users_valid_quizes(self):
        """returns a sorted list of all valid queries of all users. Used for the leaderboard endpoint"""
        query = render_sql_template(get_project_root() / "be/sql/get_all_users_valid_quizes.sql")
        return self.query(query).fetchall()

    # Users stuff
    def create_or_update_user(self, user_name: str, display_name: str):
        """creates a new user, if it doesn't exist, otherwise it updates it"""
        query = render_sql_template(get_project_root() / "be/sql/create_or_update_user.sql",
                                    user_name=user_name, display_name=display_name)
        self.query(query, blocking=True)

    def get_id_from_user(self, user_name: str) -> int | Exception:
        """returns the id of the user, if it exists"""
        res = self.query(f"select id from users where user_name='{user_name}'").fetchall()
        assert len(res) in (0, 1), f"Cannot happen due to DB restrictions: {res}"
        return int(res[0][0]) if len(res) == 1 else UserNotExistsException("User does not exist")

    def get_user_from_id(self, user_id: int) -> str | Exception:
        """returns the user from a user id, if it exists"""
        res = self.query(f"select user_name from users where id='{user_id}'").fetchall()
        assert len(res) in (0, 1), f"Cannot happen due to DB restrictions: {res}"
        return res[0][0] if len(res) == 1 else UserNotExistsException("User does not exist")

    def get_display_name_from_name(self, user_name: str) -> str | Exception:
        """returns the display_name for a user_id, if it exists"""
        if isinstance(user_id := self.get_id_from_user(user_name), UserNotExistsException):
            return user_id
        res = self.query(f"select display_name from users where user_name='{user_name}'").fetchall()
        assert len(res) == 1, f"Cannot happen due to DB restrictions {res}"
        return res[0][0]

    def check_if_user_exists(self, user_name: str) -> bool | Exception:
        """returns true if user exists, false otherwise"""
        if isinstance(user_id := self.get_id_from_user(user_name), UserNotExistsException):
            return False
        if isinstance(user_id, Exception):
            return Exception
        return True

    # user sessions stuff
    def get_user_session_token(self, user_name: str) -> SessionToken | Exception:
        """returns the user token, if it exists in the session tokens table, for a given user name"""
        if isinstance(user_id := self.get_id_from_user(user_name), Exception):
            return user_id
        if user_id is None:
            return UserNotExistsException(f"User: '{user_name}' does not exist")

        query = render_sql_template(get_project_root() / "be/sql/get_user_session_token.sql", user_id=user_id)
        res = self.query(query).fetchall()
        assert len(res) in (0, 1), "Cannot happen due to DB restrictions"
        if len(res) == 0:
            return UserNotExistsException(f"User: '{user_name}' has no session tokens in DB")

        try:
            token, valid_until = res[0][0], datetime.fromisoformat(res[0][1])
        except Exception as e:
            return e
        token = SessionToken(value=token, valid_until=valid_until)
        logger.debug(f"Token for {user_name=} ({token.value=}) Valid? {token.is_valid()}")
        return token

    def insert_session_token(self, session_token: SessionToken) -> str | Exception:
        """inserts a new session token for a user name"""
        if isinstance(user_id := self.get_id_from_user(session_token.user_name), Exception):
            return user_id
        query = render_sql_template(get_project_root() / "be/sql/insert_session_token.sql", user_id=user_id,
                                    session_token=session_token.value, created_at=to_ms(session_token.created_at),
                                    valid_until=to_ms(session_token.valid_until))
        self.query(query, blocking=True)
        return None

    def check_if_user_has_valid_session_token(self, user_name: str) -> bool | Exception:
        """returns only true/false if token exists and is valid. Throws on common errors"""
        token_res = self.get_user_session_token(user_name)
        if isinstance(token_res, Exception):
            return False if f"{token_res}".endswith("has no session tokens in DB") else token_res
        return token_res.is_valid()

    def invalidate_session_token(self, session_token: SessionToken):
        """invalidates a session token for a user name. Called at logout time"""
        self.refresh_session_token(session_token, to_ms(datetime.now(tz.utc).replace(tzinfo=None)))

    def refresh_session_token(self, session_token: SessionToken, valid_until: str):
        """refreshes a session token up until some duration. Refreshes happen at /get_quiz requests usually"""
        query = render_sql_template(get_project_root() / "be/sql/refresh_session_token.sql",
                                    valid_until=valid_until, session_token=session_token.value)
        self.query(query=query, blocking=True)

    # Private methods
    def _check_and_make_tables(self):
        self.query("create table IF NOT EXISTS qa (id integer primary key unique, question text, answers text, "
                    "correct_answers text, image blob)")
        self.query("create table IF NOT EXISTS questionnaires (id text primary key unique, user_id integer, "
                    "date_start text, date_end text, qa_ids text, response_list text, correct_response_list text)")
        self.query("create table IF NOT EXISTS users (id integer primary key AUTOINCREMENT, user_name text unique, "
                    "display_name text)")
        self.query("create table IF NOT EXISTS session_tokens (id integer primary key AUTOINCREMENT, user_id text, "
                    "session_token text, created_at text, valid_until text)")

    def __repr__(self):
        return f"[{parsed_str_type(self)}] {repr(self.db_handler)}"
