insert into users(id, user_name, display_name)
values(null, '{{ user_name }}', '{{ display_name }}')
on CONFLICT(user_name) do
    update set display_name='{{ display_name }}' -- 'CONFLICT', not 'conflict' for some reason
