select q.*, u.display_name as displayName
from questionnaires q
    inner join users u on q.user_id = u.id
where date_end is not null and response_list is not null
order by user_id
