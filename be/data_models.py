"""Data models for questions, answers and responses"""
from __future__ import annotations
from dataclasses import dataclass
from datetime import datetime, timezone as tz
from typing import Any
from utils import to_ms

@dataclass
class QuestionAnswers:
    """QA dataclass for DRPCIV"""
    qid: int
    question: str
    answers: list[str]
    correct_answers: list[int]
    image: str | None

    def to_dict(self, include_correct_answers: bool = False) -> dict[str, Any]:
        """Formats the data as a jsonable dict to be sent as a HTTP response"""
        return {
            "qid": self.qid, "question": self.question, "answers": self.answers, "image": self.image,
            **({"correct_answers": self.correct_answers} if include_correct_answers else {})
        }

@dataclass
class QuizResult:
    """quiz result dataclass after an evaluation of a QuizResponse w.r.t expected answers"""
    valid: bool
    score: int | None = None
    error: str | None = None
    correct_answers: list[bool] | None = None
    correct_answers_list: list[list[int]] | None = None

@dataclass
class QuizResponse:
    """Quiz response as sent by the user"""
    id: str
    user_answers: list[list[int]]
    date_end: datetime.date | None = None
    date_end_fmt: str | None = None

    def __post_init__(self):
        self.user_answers = [[] if x is None else x for x in self.user_answers]
        self.date_end = datetime.now(tz.utc).replace(tzinfo=None)
        self.date_end_fmt = to_ms(self.date_end)

    def evaluate(self, date_start: datetime.date, expected_answers: list[list[int]], max_duration_s: int) -> QuizResult:
        """evaluate this QuizResposnes object (constructed from frontend data) against the expecte answers"""
        if (n_expected := len(expected_answers)) != (n_provided := len(self.user_answers)):
            return QuizResult(valid=False, error=f"Expected {n_expected} answers for the quiz. Got {n_provided}")
        if any(any((x < 0) or (x > 2) for x in y) for y in self.user_answers):
            return QuizResult(valid=False, error="All answers must be between [0:2] meaning A, B or C.")

        correct_answers = list((x == y for x, y in zip(self.user_answers, expected_answers)))
        valid = (self.date_end - date_start).seconds / 60 < max_duration_s
        return QuizResult(valid=valid, correct_answers=correct_answers,
                          correct_answers_list=expected_answers, score=sum(correct_answers))
