"""Various utility functions for DRPCIV backend"""
from __future__ import annotations
from datetime import datetime, timezone as tz, timedelta as td
from dataclasses import dataclass
from pathlib import Path
from typing import Any
import hashlib
from jinja2 import Environment, FileSystemLoader, StrictUndefined
from loggez import loggez_logger as logger

@dataclass
class SessionToken:
    """Session token object"""
    value: str
    valid_until: datetime
    created_at: datetime | None = None
    user_name: str | None = None

    def __post_init__(self):
        self.valid_until = self.valid_until.replace(tzinfo=None)
        self.valid_fmt = to_ms(self.valid_until)

    def is_valid(self) -> bool:
        """returns whether this session token is valid w.r.t utcnow()"""
        return datetime.now(tz.utc).replace(tzinfo=None) <= self.valid_until

    def __eq__(self, other: str | SessionToken) -> bool:
        assert isinstance(other, (str, SessionToken)), type(other)
        return self.value == (other.value if isinstance(other, SessionToken) else other)

def get_project_root() -> Path:
    """returns the root of this project for testing/sanity paths"""
    return Path(__file__).parents[1].absolute()

def make_quiz_id(user: str, date_start: datetime.date, qa_ids: list[int]) -> str:
    """returns a unique test id based on username, the time when the query was sent (date_start) and the list of qs"""
    assert isinstance(user, str) and isinstance(date_start, datetime) \
        and isinstance(qa_ids, list), (type(user), type(date_start), type(qa_ids))
    return hashlib.md5(f"{user}_{to_ms(date_start)}_{tuple(qa_ids)}".encode("utf-8")).hexdigest()[0:10]

def make_session_token(user: str, token_duration_s: float) -> SessionToken:
    """return a unique session token based on user and date_login."""
    assert isinstance(user, str) and isinstance(token_duration_s, (int, float)), (type(user), type(token_duration_s))
    date_login = datetime.now(tz.utc).replace(tzinfo=None)
    valid_until = date_login + td(seconds=token_duration_s)
    value = hashlib.md5(f"{user}_{to_ms(date_login)}".encode("utf-8")).hexdigest()[0:10]
    return SessionToken(value=value, created_at=date_login, valid_until=valid_until, user_name=user)

def to_ms(date: datetime) -> str:
    """used to convert a datetime date to a milisecond string used by the DB or the API responses"""
    return date.isoformat(timespec="milliseconds")

def parsed_str_type(item: Any) -> str:
    """Given an object with a type of the format: <class 'A.B.C.D'>, parse it and return 'A.B.C.D'"""
    return str(type(item)).rsplit(".", maxsplit=1)[-1][0:-2]

def render_sql_template(sql_file_path: str | Path, **kwargs) -> str:
    """
    Renders a SQL template file with the given arguments using jinja.
    The SQL file path can be absolute or relative to the current working directory.
    """
    sql_file_path = Path(sql_file_path).absolute()
    env = Environment(loader=FileSystemLoader(sql_file_path.parent), undefined=StrictUndefined)
    template = env.get_template(sql_file_path.name)
    query = template.render(**kwargs)
    logger.debug(f"Rendered query: \n{query}")
    return query

def format_list_for_query(arr: list[int | str]) -> list[str]:
    """
    Given a list of integers of strings, format it to be used in a query directly
    Example: arr: [1, 2, "asdf"] becomes the string: "1, 2, asdf"
    """
    assert isinstance(arr, list) and all(isinstance(element, (int, str)) for element in arr), arr
    return ",".join(map(str, arr))
