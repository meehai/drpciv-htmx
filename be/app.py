#!/usr/bin/env python3
"""flask server for DRPCIV"""
from __future__ import annotations
from argparse import ArgumentParser, Namespace
from pathlib import Path
import os
from datetime import datetime, timezone as tz, timedelta as td
from flask import Flask, Response, render_template, url_for, request, jsonify, current_app as app
from flask_cors import CORS
from loggez import loggez_logger as logger

from auth import validate_session_token_request, setup_google_auth, login, logout, google_login_callback, \
    check_token_status
from db_handler import LibSQLDBHandler, SQLiteDBHandler
from drpciv_db_handler import DRPCIVDBHandler
from utils import make_quiz_id, to_ms
from data_models import QuizResponse

_APP: Flask | None = None # pylint: disable=global-statement
os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1" # to allow Http traffic for local dev

# pylint: disable=no-member, global-statement, protected-access
def _setup_app(db_engine: str, db_path: str, fe_path: Path, auth_type: str, quiz_duration_s: int,
               session_token_duration_s: int, google_oauth_path: Path | None = None,
               turso_token_path: Path | None = None) -> Flask:
    global _APP
    if _APP is not None:
        return _APP
    _app = Flask(__name__, template_folder=fe_path, static_folder=fe_path / "static")
    # workaround for having the app in subpath (i.e. meehai.xyz/drpciv/xxx)
    _app.jinja_env.globals["url_for"] = lambda *args, **kw: f"{os.getenv('URL_PREFIX', '')}{url_for(*args, **kw)}"
    logger.info(f"URL prefix: {os.getenv('URL_PREFIX', '')}")
    if db_engine == "libsql":
        engine_handler = LibSQLDBHandler(db_path, token_path=turso_token_path)
    else:
        engine_handler = SQLiteDBHandler(db_path)
    _app.db_handler = DRPCIVDBHandler(engine_handler)
    CORS(_app)
    if auth_type == "google_auth":
        assert os.getenv("REDIRECT_URI") is not None, "For google auth, REDIRECT_URI must be set"
        setup_google_auth(_app, google_oauth_path, redirect_uri=f"{os.environ['REDIRECT_URI']}/google_login_callback")

    # Config setup. TODO: read from a yaml file
    _app.config["TEMPLATES_AUTO_RELOAD"] = True
    _app.config["auth_type"] = auth_type
    _app.config["quiz_duration_s"] = quiz_duration_s
    _app.config["session_token_duration_s"] = session_token_duration_s

    # endpoints
    _app.add_url_rule("/", "index", view_func=index)
    _app.add_url_rule("/get_quiz", "get_quiz", view_func=get_quiz, methods=["GET"])
    _app.add_url_rule("/evaluate", "evaluate", view_func=evaluate, methods=["POST"])
    _app.add_url_rule("/leaderboard", "leaderboard", view_func=leaderboard, methods=["GET", "POST"])
    _app.add_url_rule("/create_user", "create_user", view_func=create_user, methods=["PUT"])
    _app.add_url_rule("/update_display_name", "update_display_name", view_func=update_display_name, methods=["PUT"])
    _app.add_url_rule("/login", "login", view_func=login, methods=["GET", "POST"])
    _app.add_url_rule("/check_token_status", "check_token_status", view_func=check_token_status, methods=["GET"])
    _app.add_url_rule("/google_login_callback", "google_login_callback", view_func=google_login_callback,
                      methods=["GET"])
    _app.add_url_rule("/logout", "logout", logout, methods=["GET"])
    _app.add_url_rule("/profile", "profile", view_func=profile, methods=["GET"])

    return (_APP := _app)

def get_quiz() -> Response:
    """returns a new quiz or the existing one for the given user"""
    if "user" not in request.values:
        return {"error": "user=... not in request url"}, 400 # TODO: add unit tests for this
    if isinstance(token := validate_session_token_request(user := request.values["user"]), Exception):
        return {"error": f"Authentication error: {token}"}, 400
    if isinstance(user_id := app.db_handler.get_id_from_user(user), Exception):
        return {"error": str(user_id)}, 400 # TODO: add unit tests for this

    now = datetime.now(tz.utc).replace(tzinfo=None)
    app.db_handler.refresh_session_token(token, now + td(seconds=app.config["session_token_duration_s"] + \
                                                         app.config["quiz_duration_s"]))
    quiz_id = None
    if (active_test := app.db_handler.get_active_quiz_by_user_name(user, app.config["quiz_duration_s"])) is not None:
        quiz_id, date_start, qa_ids = active_test
        max_time = app.config["quiz_duration_s"]
        time_left = f"{(max_time - (now - date_start).seconds) // 60}:{(max_time - (now - date_start).seconds) % 60}"
        logger.info(f"Returning an existing test that was not finished, {quiz_id=}, {time_left=}")
        quiz_data = app.db_handler.get_questions(mode="fixed_ids", ids=qa_ids)
    else:
        quiz_data = app.db_handler.get_questions(mode="random", n=26)
        qa_ids, qa_correct_answers = [qa.qid for qa in quiz_data], [qa.correct_answers for qa in quiz_data]
        quiz_id = make_quiz_id(user, now, qa_ids)
        logger.info(f"Generated test ({to_ms(now)}) with {quiz_id=}")
        date_start = now
        app.db_handler.insert_new_quiz(quiz_id, user_id, to_ms(date_start), qa_ids, qa_correct_answers)
    return {"id": quiz_id, "user": user, "dateStart": to_ms(date_start),
            "questionsAnswers": [qa.to_dict() for qa in quiz_data]}, 200

def evaluate() -> Response:
    """evaluates a quiz given provided answers"""
    data = request.get_json()
    if "id" not in data:
        return {"error": "`id` property not in the provided request. Cannot evaluate"}, 400
    quiz_data = app.db_handler.get_quiz_by_id(data["id"])
    if len(quiz_data) == 0:
        return {"error": f"`id` '{data['id']}' is not an active quiz. Cannot evaluate."}, 400
    if len(quiz_data) > 1:
        return {"error": f"`id` '{data['id']}' is somehow duplicate. Cannot evaluate."}, 400
    quiz_data = quiz_data[0]
    if isinstance(user := app.db_handler.get_user_from_id(quiz_data[1]), Exception):
        return {"error": str(user)}, 400
    if isinstance(err := validate_session_token_request(user), Exception):
        return {"error": f"Authentication error: {err}"}, 400
    if "questionsResponses" not in data:
        return {"error": "`questionsResponses` property not in the provided request. Cannot evaluate"}, 400

    quiz_response = QuizResponse(id=data["id"], user_answers=data["questionsResponses"])
    expected_answers = [list(map(int, x.split(","))) for x in quiz_data[6].split("|")] # [ [0], [1,2], [0] etc. ]
    date_start = datetime.fromisoformat(quiz_data[2])
    res = quiz_response.evaluate(date_start, expected_answers, app.config["quiz_duration_s"])
    if res.error is not None:
        return {"error": res.error}, 400

    app.db_handler.update_active_quiz(data["id"], quiz_response.user_answers, quiz_response.date_end_fmt)
    logger.info(f"Evaluated test: {data['id']}. Score: {res.score}. Valid: {res.valid}")
    return {"correctAnswers": res.correct_answers, "correctAnswersList": res.correct_answers_list,
            "score": res.score, "valid": res.valid}, 200

def leaderboard() -> Response:
    """leaderboard page"""
    if request.method == "GET":
        return render_template("leaderboard.html")

    def _groupby_aggregate_users_data(data) -> list[dict]:
        i = 0
        res = []
        while i < len(data): # manual groupby based on n_cnt
            current_user = data[i][7]
            j = 1
            while i + j < len(data) and data[i + j][7] == current_user:
                j += 1
            user_scores, user_rows = [], data[i: i + j]
            for row in user_rows:
                date_start, date_end = datetime.fromisoformat(row[2]), datetime.fromisoformat(row[3])
                if (date_end - date_start).total_seconds() > app.config["quiz_duration_s"]:
                    user_scores.append(0)
                else:
                    response_list, correct_response_list = row[5].split("|"), row[6].split("|")
                    score = sum(_x == _y for _x, _y in zip(response_list, correct_response_list))
                    user_scores.append(score)
            res.append({"displayName": current_user, "numTests": len(user_rows), "totalScore": sum(user_scores),
                        "avg": round(sum(user_scores) / len(user_scores), 2)})
            i = i + j
        return sorted(res, key=lambda user_data: user_data["avg"], reverse=True)

    data = app.db_handler.get_all_users_valid_quizes()
    res = _groupby_aggregate_users_data(data)
    return jsonify(res), 200

def create_user() -> Response:
    """creates or updates a user. Returns the user_id"""
    data = request.get_json()
    if set(data.keys()) != {"user", "displayName"}:
        return {"error": f"Expected 'user' and 'displayName' keys, got: {list(data.keys())}"}, 400
    user, display_name = data["user"], data["displayName"]
    new_user = not app.db_handler.check_if_user_exists(user)
    app.db_handler.create_or_update_user(user_name=user, display_name=display_name)
    logger.debug(f"{'Created' if new_user else 'Updated'} new user: {user} (display name: {display_name}")
    return {"newUser": new_user}, 200

def update_display_name() -> Response:
    """updates the display name of a user. TODO: make tests"""
    data = request.get_json()
    if set(data.keys()) != {"user", "newDisplayName"}:
        return {"error": f"Expected 'user' and 'newDisplayName' keys, got: {list(data.keys())}"}, 400
    if isinstance(token := validate_session_token_request(user := data["user"]), Exception):
        return {"error": f"Authentication error: {token}"}, 400
    user, new_display_name = data["user"], data["newDisplayName"]
    assert app.db_handler.check_if_user_exists(user), "cannot happen"
    app.db_handler.create_or_update_user(user_name=user, display_name=new_display_name)
    return {"newDisplayName": new_display_name}

def index() -> Response:
    """index html"""
    return render_template("index.html")

def profile() -> Response:
    """profile html"""
    return render_template("profile.html")

def get_args() -> Namespace:
    """cli args"""
    parser = ArgumentParser()
    parser.add_argument("--db_path", type=str, required=True)
    parser.add_argument("--db_engine", type=str)
    # Flask cli ags
    parser.add_argument("--host", default="127.0.0.1")
    parser.add_argument("--port", type=int, default=42069)
    parser.add_argument("--use_reloader", action="store_true")
    parser.add_argument("--fe_path", type=Path, default=Path(__file__).absolute().parents[1] / "fe")
    # Auth cli args
    parser.add_argument("--auth_type", choices=["google_auth", "local_auth"], default="local_auth")
    parser.add_argument("--google_oauth_path", type=Path, help="If set, google auth is required, otherwise local auth")
    # Turso args
    parser.add_argument("--turso_token_path", type=Path)
    # Configs about this app
    parser.add_argument("--session_token_duration_s", type=int, default=60 * 60 * 24)
    parser.add_argument("--quiz_duration_s", type=int, default=60 * 30)
    args = parser.parse_args()
    if args.auth_type == "google_auth":
        assert (pth := args.google_oauth_path) is not None and pth.exists(), f"'{pth}' doesn't exist."
    if args.db_engine is None:
        if args.db_path.find("libsql://") != -1:
            logger.debug("'libsql://' DB. Defaulting engine to 'libsql'")
            args.db_engine = "libsql"
        else:
            args.db_engine = "sqlite"
    assert args.db_engine in ("libsql", "sqlite"), f"Unknown db engine: '{args.db_engine}'"
    if args.db_engine == "libsql" and args.db_path.find("libsql://") != -1: # remote/cloud
        assert (pth := args.turso_token_path) is not None and pth.exists(), f"'{pth}' doesn't exist."
    return args

def main(args: Namespace):
    """main fn"""
    args = get_args()
    curr_app = _setup_app(args.db_engine, args.db_path, args.fe_path, args.auth_type,
                          google_oauth_path=args.google_oauth_path, quiz_duration_s=args.quiz_duration_s,
                          session_token_duration_s=args.session_token_duration_s,
                          turso_token_path=args.turso_token_path)
    curr_app.run(host=args.host, port=args.port, use_reloader=args.use_reloader)

if __name__ == "__main__":
    main(get_args())
