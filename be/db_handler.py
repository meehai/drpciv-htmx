"""Handles QA libsql (sqlite) database queries, inserts, checks etc"""
# pylint: disable=expression-not-assigned
from __future__ import annotations
from multiprocessing import Lock
from abc import ABC, abstractmethod
from pathlib import Path
import sqlite3
import json
from loggez import loggez_logger as logger
try:
    import libsql_experimental as libsql
except ImportError:
    logger.warning("`libsql` package not installed. Cannot use LibSQLDBHandler. Use SQLiteDBHandler")

from utils import parsed_str_type

class DBHandler(ABC):
    """Generic DB Handler for sqlite engines"""
    def __init__(self, path: str):
        self.path = path
        self.db_lock = Lock()
        logger.info(f"Connected to DB. Path: {self.path}")

    @property
    @abstractmethod
    def conn(self) -> "Connection":
        """The connection to the DB. Each handler must implement its variant of this"""

    @abstractmethod
    def query(self, query: str, blocking: bool=False) -> "Cursor":
        """Queries the DB. Locks if needed."""

    def table_exists(self, table_name: str) -> bool:
        """checks if a table exists"""
        query = f"select name from sqlite_master WHERE type='table' AND name='{table_name}'"
        return len(self.conn.execute(query).fetchall()) == 1

    def __repr__(self):
        return f"[{parsed_str_type(self)}]. Path: '{self.path}'."

class LibSQLDBHandler(DBHandler):
    """LibSQL implementation of the DB Handler"""
    def __init__(self, path: str, token_path: Path | None = None):
        if path.find(":") == -1:
            logger.debug(f"'{path}' doesnt have libsql:// or file:. Adding 'file:'")
            path = f"file:{path}"
        self.auth_token = json.load(open(token_path, "r"))["token"] if token_path is not None else ''
        self._conn = libsql.connect(path, check_same_thread=False, auth_token=self.auth_token)
        super().__init__(path)

    @property
    def conn(self) -> libsql.Connection:
        return self._conn

    def query(self, query: str, blocking: bool=False) -> libsql.Cursor:
        """just a wrapper to run a query"""
        logger.debug2(f"Query: '{query}'")
        self.db_lock.acquire() if blocking else None
        try:
            res = self.conn.execute(query)
            self.conn.commit()
        except ValueError as e:
            if "Hrana" in str(e) and hasattr(self, "_retry_turso_conn"):
                logger.debug(f"Turso failed: {e}. Retrying to connect...")
                self._retry_turso_conn() # TODO: turso maybe fix this?
                res = self.conn.execute(query)
                self.conn.commit()
            else:
                self.db_lock.release() if blocking else None
                raise e
        self.db_lock.release() if blocking else None
        return res

    def _retry_turso_conn(self):
        self._conn = libsql.connect(self.path, check_same_thread=False, auth_token=self.auth_token)

class SQLiteDBHandler(DBHandler):
    """SQLite implementation of the DB Handler"""
    def __init__(self, path: Path):
        self._conn = sqlite3.connect(path, check_same_thread=False)
        super().__init__(path)

    @property
    def conn(self) -> sqlite3.Connection:
        return self._conn

    def query(self, query, blocking = False):
        logger.debug2(f"Query: '{query}'")
        self.db_lock.acquire() if blocking else None
        res = self.conn.execute(query)
        self.conn.commit()
        self.db_lock.release() if blocking else None
        return res
