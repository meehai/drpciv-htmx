"""authentication module"""
import os
import json
from pathlib import Path
from flask import Response, render_template, request, redirect, abort, session, current_app as app, Flask
from loggez import loggez_logger as logger

try:
    import requests
    from google.oauth2 import id_token
    from google_auth_oauthlib.flow import Flow
    from pip._vendor import cachecontrol
    import google.auth.transport.requests
except ImportError as e:
    logger.warning("Error importing google auth requirements. Install requirements_prod.txt.")

from utils import make_session_token, SessionToken, to_ms

def validate_session_token_request(user: str) -> SessionToken | Exception:
    """checks if the user is logged in for some pages. Returns the user's session token if is logged in."""
    token: SessionToken | Exception
    lowercase_headers = {k.lower(): v for k, v in request.headers.items()}
    if "x-drpcivflask-sessiontoken" not in lowercase_headers:
        logger.debug(f"'X-DRPCIVFlask-SessionToken' not in the header of request: {request.headers}")
        return Exception("'X-DRPCIVFlask-SessionToken' not in the header of request")
    if isinstance(token := app.db_handler.get_user_session_token(user), Exception):
        logger.debug(token)
        return Exception(token)
    if (x := lowercase_headers["x-drpcivflask-sessiontoken"]) != (y := token):
        logger.debug(f"Token exists, but is invalid {x} vs {y}")
        return Exception("Invalid token")
    if not token.is_valid():
        logger.debug(f"Token exists, but is expired ({token.valid_until=})")
        return Exception("Expired token")
    return token

def setup_google_auth(_app: Flask, google_oauth_path: Path, redirect_uri: str):
    """setup the google auth required stuff from config file json. Called before .run(), so app needs to be sent."""
    assert "REDIRECT_URI" in os.environ, "'REDIRECT_URI' env variable must be set for google auth setup"
    logger.debug(f"Setting up google auth from '{google_oauth_path}'. Redicect URI: '{redirect_uri}'")
    with open(google_oauth_path, "r") as json_file:
        client_config = json.load(json_file)
    _app.GOOGLE_CLIENT_ID = client_config["web"]["client_id"]
    _app.secret_key = client_config["web"]["client_secret"]
    client_config["web"]["redirect_uris"] = [redirect_uri]

    _app.flow = Flow.from_client_config(
        client_config=client_config,
        scopes=["https://www.googleapis.com/auth/userinfo.profile",
                "https://www.googleapis.com/auth/userinfo.email", "openid"],
        redirect_uri=redirect_uri
    )

def login() -> Response:
    """login endpoint. GET for google oauth/local auth. POST only for local auth for the form"""
    if request.method == "GET":
        if app.config["auth_type"] == "local_auth":
            return render_template("login.html", auth_type="local_auth")
        url, state = app.flow.authorization_url() # google auth
        session["state"] = state
        return redirect(url)

    # POST call to this endpoint happens from the login.html button pressing, not google auth.
    login_data = request.get_json()
    assert app.config["auth_type"] == "local_auth", app.config["auth_type"]
    if (keys := set(login_data.keys())) != {"user"}:
        return {"error": f"expected 'user' key for 'login/' endpoint. Got: {list(keys)}"}, 400
    if isinstance(display_name := app.db_handler.get_display_name_from_name(user := login_data['user']), Exception):
        logger.debug(f"error at local auth login: {display_name}")
        return {"error": str(display_name)}, 400
    logger.debug(f"Auth type: 'local_auth'. Setting session name to: '{user}' ({display_name=})")
    return post_login(user=user, display_name=display_name)

def google_login_callback() -> Response:
    """callback that is called by the google auth services back to our app"""
    try:
        app.flow.fetch_token(authorization_response=request.url)
    except Exception as e:
        logger.debug(f"Error on auth: '{e}'")
        abort(500)
    if not session["state"] == request.args["state"]:
        abort(500)

    credentials = app.flow.credentials
    request_session = requests.session()
    cached_session = cachecontrol.CacheControl(request_session)
    token_request = google.auth.transport.requests.Request(session=cached_session)

    id_info = id_token.verify_oauth2_token(
        id_token=credentials._id_token, # pylint: disable=protected-access
        request=token_request,
        audience=app.GOOGLE_CLIENT_ID
    )
    user, display_name = id_info.get("email"), id_info.get("name")
    if isinstance(check := app.db_handler.check_if_user_exists(user), Exception):
        logger.debug(f"internal error at post_login: {check}")
        return {"error": f"{check}"}, 400
    if check is False:
        logger.debug(f"New google user: {user=}, {display_name=}. Creating in DB")
        app.db_handler.create_or_update_user(user_name=user, display_name=display_name)
    else:
        display_name = app.db_handler.get_display_name_from_name(user)
        assert isinstance(display_name, str), f"Expected str, got: {display_name} {type(display_name)}"
        logger.debug(f"Existing google user: {user=} with {display_name=}")
    res, status_code = post_login(user, display_name)
    if status_code != 200:
        return res, status_code
    return render_template("login.html", auth_type="google_auth", **res)

def post_login(user: str, display_name: str) -> Response:
    """post login actions. Create a unique session token and return a json so the client knows it's logged in."""
    if isinstance(check := app.db_handler.check_if_user_exists(user), Exception):
        logger.debug(f"internal error at post_login: {check}")
        return {"error": f"{check}"}, 400
    if check is False:
        return {"error": f"user '{user}' doesn't exist. Use '/create_user' endpoint of frontend"}, 400
    if isinstance(has_token := app.db_handler.check_if_user_has_valid_session_token(user), Exception):
        logger.debug(f"internal error at post_login: {has_token}")
        return {"error": f"{has_token}"}, 400
    if has_token:
        token = app.db_handler.get_user_session_token(user)
        logger.debug(f"Already logged in: {user=}, {display_name=}")
    else:
        token = make_session_token(user, app.config["session_token_duration_s"])
        err = app.db_handler.insert_session_token(token)
        if err is not None:
            return {"error": "Internal error"}
        logger.debug(f"Logged in: {user=}, {display_name=}, {token=}")
    return {"auth": True, "user": user, "displayName": display_name, "sessionToken": token.value}, 200

def check_token_status() -> Response:
    """checks if the token passed in the headers is valid or not for a given user."""
    token: SessionToken | Exception
    if "user" not in request.values:
        return {"error": "user=... not in request url"}, 400
    if isinstance(token := validate_session_token_request(user := request.values["user"]), Exception):
        logger.debug(f"Checked token status for {user=}. Got an auth error: {token}")
        return {"error": f"Authentication error: {token}"}, 400
    logger.debug(f"Checked token status for {user=}. Valid: {token.is_valid()}")
    return {"valid": token.is_valid(), "valid_until": None if not token.is_valid() else to_ms(token.valid_until)}, 200

def logout():
    """logout endpoint"""
    if "user" not in request.values:
        return {"error": "user=... not in request url"}, 400
    if isinstance(token := validate_session_token_request(user := request.values["user"]), Exception):
        logger.debug(f"Checked token status for {user=}. Got an auth error: {token}")
        return {"error": f"Authentication error: {token}"}, 400
    app.db_handler.invalidate_session_token(token)
    return {"logOut": True}, 200
