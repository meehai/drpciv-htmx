# DRPCIV Flask

We learn DPRCIV. We learn Flask. We learn teamwork.

To install all the python dependencies, use `pip install -r requirements.txt`.

Note, it's recommended to use `git update-index --assume-unchanged data/drpciv.db` so you don't push changes to the DB.

## Starting the HTTP server

```bash
python be/app.py --db_path data/drpciv.db [--host HOST] [--port PORT] [--use_reloader]
```

### Test the server via curl:
```bash
curl -X PUT -H "Content-Type: application/json" -d '{"user": "some_name", "displayName": "some_name"}' 127.0.0.1:42069/create_user
sessionToken=$(curl -X POST -H "Content-Type: application/json" -d '{"user": "some_name"}' localhost:42069/login 2>/dev/null | jq -r ".sessionToken")
quizData=$(curl 127.0.0.1:42069/get_quiz?user=some_name\&sessionToken="$sessionToken" 2>/dev/null)
quizId=$(echo $quizData | jq -r ".id")
curl -X POST -H "Content-Type: application/json" -d "{\"user\": \"some_name\", \"sessionToken\": \"$sessionToken\", \"id\":\"$quizId\",\"questionsResponses\":[[2],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1]]}" localhost:42069/evaluate # 26 answers in the format above
```

### Running the tests
```bash
pytest test/
```

#### End to end test using curl
```bash
bash .ci/run_curl.sh
```

### Using Google Auth for local development
```bash
REDIRECT_URI=http://localhost:42069 LOGGEZ_LOGLEVEL=2 python be/app.py --db_path data/drpciv.db \
    --auth_type google_auth --google_oauth_path .secrets/client_secret.json
```

In order to get the secret file, ask the maintainer.

### Using turso sqlite cloud database for local development
```bash
LOGGEZ_LOGLEVEL=2 python be/app.py --db_path libsql://drpciv-test-meehai.turso.io \
    --turso_token_path .secrets/turso_token.json
```
In order to get the credentials of this DB, ask the maintainer. Otherwise, make your own DB on the turso website.
The format is `{"token": TOKEN}`.

## Data Ingestion part

In order to do this, we run a simple command:

```bash
python data/ingest.py -o data/drpciv.db [--force]
```

This will check the DB for missing questions and request the missing ones. The first request simply checks for the
N+1th question and sees if it's empty. If so, then it will request 1 by 1 until an empty response is returned. If using
`--force` then it will start from scratch.

Good luck!

<img src='logo.jpg' alt='pisi' width='250'/>
