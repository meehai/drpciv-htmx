From: [link](https://www.wallacesharpedavidson.nz/post/sqlite-cloudrun/)

Run locally first time (create project and stuff)


```
gcloud auth login
gcloud auth login application-default
gcloud auth configure-docker europe-central2-docker.pkg.dev
gcloud storage buckets create gs://drpciv-flask-bucket --project=drpciv-flask-cloudrun --location=eu
gsutil cp data/drpciv.db gs://drpciv-flask-bucket

# these two below must be ran at every commit
docker build -t europe-central2-docker.pkg.dev/drpciv-flask-cloudrun/drpciv-flask-registry/drpciv-flask:latest -f .cd/Dockerfile .
docker push europe-central2-docker.pkg.dev/drpciv-flask-cloudrun/drpciv-flask-registry/drpciv-flask:latest

gcloud beta run deploy drpciv-flask-cloudrun \
  --image europe-central2-docker.pkg.dev/drpciv-flask-cloudrun/drpciv-flask-registry/drpciv-flask:latest --add-volume=name=sqlite-test,type=cloud-storage,bucket=drpciv-flask-bucket \
  --add-volume-mount=volume=sqlite-test,mount-path=/app/data \
  --execution-environment=gen2 \
  --allow-unauthenticated \
  --port=5000 --region europe-central2
```

Then

```
gcloud iam service-accounts keys create key.json --iam-account=534874435612-compute@developer.gserviceaccount.com
# Add contents of key.json in CI/CD variables in gitlab ui (not as type file)
gcloud projects add-iam-policy-binding 'drpciv-flask-cloudrun' \
  --member='serviceAccount:534874435612-compute@developer.gserviceaccount.com' \
  --role='roles/artifactregistry.reader'

gcloud projects add-iam-policy-binding 'drpciv-flask-cloudrun' \
  --member='serviceAccount:534874435612-compute@developer.gserviceaccount.com' \
  --role='roles/artifactregistry.writer'
```

Finally, in .gitlab-ci.yml:

```
push_to_google_cloudrun:
  rules:
    # - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_REF_NAME == "master"'
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "main" # for debug
  image: gcr.io/google.com/cloudsdktool/google-cloud-cli:466.0.0-alpine
  services:
    - docker:24.0.5-dind
  variables:
    DOCKER_HOST: tcp://docker:2375
    GOOGLE_APPLICATION_CREDENTIALS: /root/key.json
  before_script:
    - echo "${GCLOUD_KEY_JSON}" > "${GOOGLE_APPLICATION_CREDENTIALS}"
    - gcloud auth activate-service-account --key-file "${GOOGLE_APPLICATION_CREDENTIALS}"
    - gcloud auth configure-docker europe-central2-docker.pkg.dev
  script:
    - docker build -t europe-central2-docker.pkg.dev/drpciv-flask-cloudrun/drpciv-flask-registry/drpciv-flask:latest -f .cd/Dockerfile .
    - docker push europe-central2-docker.pkg.dev/drpciv-flask-cloudrun/drpciv-flask-registry/drpciv-flask:latest
```

