import { now, arrayFill, arraySum } from './utils.js';
import { checkIfTokenIsValid, logoutButtonOnClick } from './auth.js';

let quiz = null; // eslint-disable-line no-unused-vars

/**
 * function to check if the user is logged so we know what html to server to user (get new quiz or login)
 */
export async function loginOrStart() { // eslint-disable-line no-unused-vars
    const displayName = localStorage.getItem('displayName');
    const startContainer = document.getElementById('username-start-container')
    const user = localStorage.getItem('user')
    if (user == null || !await checkIfTokenIsValid()) {
        startContainer.innerHTML = `
<button onclick="location.href='login'" class="blue-button">Login</button>
<div id="error-msg"> </div>
`
    } else {
        startContainer.innerHTML = `
<p>
    Hello, <a href="profile"><b><span id="display-name">${displayName}</span></b></a>!
<button id="logout-button" class="red-button">Logout</button>
</p>
<button id="start-button" class="green-button">Start</button>
<div id="error-msg"> </div>
`
        document.getElementById('logout-button').addEventListener('click', logoutButtonOnClick);
        document.getElementById('start-button').addEventListener('click', startButtonOnClick);
    }
}

/**
 * Handles the onclick event from the frontend button which triggers a new quiz. Checks if the name is properly put.
 */
function startButtonOnClick() { // eslint-disable-line no-unused-vars
    const landingPageContainer = document.getElementById('landing-page-container');
    const errorMsg = document.getElementById('error-msg');
    const sessionToken = localStorage.getItem('sessionToken');
    const user = localStorage.getItem('user');

    landingPageContainer.classList.add('d-none');
    landingPageContainer.innerHTML = '';
    document.getElementById('quiz-container').innerHTML = ''
    console.log(`[${now()}] Starting a new quiz for username '${user}'.`)

    fetch(`get_quiz?user=${user}`, {
        method: 'GET',
        headers: { 'X-DRPCIVFlask-SessionToken': sessionToken }
    }).then((response) => {
        return new Promise((resolve) => response.json()
            .then((json) => resolve({
                status: response.status,
                json
            })));
    }).then(({ status, json }) => {
        const activeQuiz = localStorage.getItem('activeQuiz')
        let selectedAnswers = null;
        switch (status) {
        case 200:
            console.log(`[${now()}] Received a new quiz id: '${json.id}'.`)
            if (activeQuiz != null) {
                const _activeQuiz = JSON.parse(activeQuiz);
                if (_activeQuiz.id === json.id) {
                    console.log(`[${now()}] Resetting quiz answers from a previous run: '${json.id}'.`)
                    selectedAnswers = _activeQuiz.selectedAnswers
                }
            }
            quiz = new QuizManager(user, json, selectedAnswers); // eslint-disable-line no-new
            break
        default:
            console.log(`[${now()}] Status: ${status}. Error: '${JSON.stringify(json)}'.`)
            errorMsg.innerHTML = JSON.stringify(json)
            break
        }
    })
        .catch(error => {
            console.error(error);
        });
}

/**
 * QuizManager handles the endpoint call and the frontend quiz steps: selecting or skipping answers, keeping track of
 * time, keeping track of currently answered questions, sending results to the backend etc.
 * Current implementation creates 26 hidden divs and shuffles the visible/invisible ones via CSS based on the current
 * question.
 * @property {int} currentQuestionIndex The current question index.
 * @property {Array} selectedAnswers The list answers.
 * @property {Element} quizContainer The quiz container where the questions are displayed.
*
*/
class QuizManager {
    #currentQuestionIndex = 0;
    #quizContainer

    /**
     * Function that queries the backend for a new quiz.
     * @param {string} name - The name of the user that starts a new quiz
     * @property {Array<JSON>|null} quizData The list of questions as received from the backend when button is clicked.
     */
    constructor(name, quizData, selectedAnswers) {
        this.name = name;
        this.quizData = quizData;
        this.quizFinished = false;
        this.nQuestions = quizData.questionsAnswers.length;
        this.#quizContainer = document.getElementById('quiz-container');
        if (selectedAnswers == null) {
            this.selectedAnswers = arrayFill(this.nQuestions, null);
        } else {
            this.selectedAnswers = selectedAnswers;
        }
        // this.selectedAnswers = arrayFill(this.nQuestions, [0,2]); // debug only: prefill most answers
        // this.selectedAnswers[0] = null;

        this.nextButton = null;
        this.submitButton = null;
        this.forceFinishButton = null;
        this.#createHTMLWithData();
        this.#setHandlersAndListeners();
    }

    /**
     * Method that renders the html elements containing the data that we fetched from the API.
     */
    #createHTMLWithData() {
        // Add questions overview container
        const questionsOverviewDiv = document.createElement('div');
        questionsOverviewDiv.setAttribute('id', 'questions-overview');

        this.forceFinishButton = document.createElement('button');
        this.forceFinishButton.setAttribute('id', 'force-finish');
        this.forceFinishButton.classList.add('red-button');
        this.forceFinishButton.textContent = '✖';

        this.#quizContainer.appendChild(this.forceFinishButton);

        this.quizData.questionsAnswers.forEach((currentElement, index) => {
            // Create a div element for the question
            const dynamicContainer = document.createElement('div');
            dynamicContainer.classList.add('dynamic-container');

            // Create a div element for the question in overview (small slot)
            const questionSlot = document.createElement('div');
            questionSlot.classList.add('question-slot');

            const questionDiv = document.createElement('div');
            questionDiv.classList.add('question');

            // Set the text content of the question div
            questionDiv.textContent = 'Q' + (index + 1) + '. ' + currentElement.question;

            // Set question slot text in the overview
            questionSlot.textContent = 'Q' + (index + 1);

            // Create a div element for the answers container
            const answerContainerDiv = document.createElement('div');
            answerContainerDiv.classList.add('answer-container');

            // Create a div element for the answers
            const answersDiv = document.createElement('div');
            answersDiv.classList.add('answers');

            // Loop through the answers array of the current question
            currentElement.answers.forEach(answer => {
                // Create a div element for each answer and append it to container
                const answerDiv = document.createElement('div');
                answerDiv.classList.add('answer');
                answerDiv.textContent = answer;
                answersDiv.appendChild(answerDiv);
            });

            // Append the answers div to the answer container div
            answerContainerDiv.appendChild(answersDiv);

            // Check if there's an image available
            if (currentElement.image) {
                // If an image exists, create a div for the question image
                const questionImageDiv = document.createElement('div');
                questionImageDiv.classList.add('question-image');

                // Create an img element for the image
                const imgElement = document.createElement('img');
                imgElement.setAttribute('src', 'data:image/jpeg;base64,' + currentElement.image);
                imgElement.setAttribute('width', '270');

                // Append the image to the question image div
                questionImageDiv.appendChild(imgElement);

                // Append the question image div to the answer container div
                answerContainerDiv.appendChild(questionImageDiv);
            }

            // Append the question div and answer container div to the test container div
            this.#quizContainer.appendChild(dynamicContainer);
            questionsOverviewDiv.appendChild(questionSlot);
            dynamicContainer.appendChild(questionDiv);
            dynamicContainer.appendChild(answerContainerDiv);
        });

        // Add buttons to the test container
        const buttonsDiv = document.createElement('div');
        buttonsDiv.classList.add('test-buttons');

        this.nextButton = document.createElement('button');
        this.nextButton.setAttribute('id', 'next');
        this.nextButton.textContent = 'Next';

        this.submitButton = document.createElement('button');
        this.submitButton.setAttribute('id', 'submit-button');
        this.submitButton.classList.add('blue-button');
        this.submitButton.textContent = 'Submit';

        buttonsDiv.appendChild(document.createElement('br'));
        buttonsDiv.appendChild(this.nextButton);
        buttonsDiv.appendChild(this.submitButton);

        // Append buttons div to the test container
        this.#quizContainer.appendChild(buttonsDiv);
        this.#quizContainer.appendChild(questionsOverviewDiv);
        console.log(`[${now()}] Finished creating the hidden divs for all the questions.`)
    }

    #setHandlersAndListeners() { // eslint-disable-line require-jsdoc
        // Get all question elements
        const dynamicContainers = document.querySelectorAll('.dynamic-container');

        this.#displayCurrentQuestion();
        this.#setupQuestionSlots();

        // Add event listener to the Next button
        this.nextButton.addEventListener('click', (event) => this.#updateCurrentQuestionAndDisplayNext(event));
        this.submitButton.addEventListener('click', (event) => this.#submitQuiz(event));
        this.forceFinishButton.addEventListener('click', (event) => {
            if (confirm('Are you sure you want to submit the quiz early?')) {
                this.#submitQuiz(event)
            }
        });
        // Add click event listener to each answer
        dynamicContainers.forEach(question => {
            const answers = Array.from(question.querySelectorAll('.answer'));
            answers.forEach(answer => {
                answer.addEventListener('click', (event) => this.#handleAnswerSelection(event));
            });
        });
        console.log(`[${now()}] Finished adding all the event handlers to the buttons.`)
    }

    /**
     * Displays the current question by looping through all the other ones and removing 'current'.
     * TODO: This should not loop through all of them, but rather we should keep an index of current and prev question.
     */
    #displayCurrentQuestion() {
        const dynamicContainers = document.querySelectorAll('.dynamic-container');
        const questionSlots = document.querySelectorAll('.question-slot');
        for (let i = 0; i < this.nQuestions; i++) {
            questionSlots[i].classList.add(i === this.#currentQuestionIndex ? 'slot-selected' : 'slot-not-selected');
            questionSlots[i].classList.remove(i === this.#currentQuestionIndex ? 'slot-not-selected' : 'slot-selected');
            questionSlots[i].classList.remove('slot-answered')
            if (this.selectedAnswers[i] !== null && this.selectedAnswers[i].length > 0 && !this.quizFinished) {
                questionSlots[i].classList.add('slot-answered')
            }
            if (i !== this.#currentQuestionIndex) {
                dynamicContainers[i].classList.remove('question-selected')
                continue
            }

            dynamicContainers[i].classList.add('question-selected')

            if (this.selectedAnswers[i] === null || this.selectedAnswers[i].length === 0) {
                continue;
            }

            for (let j = 0; j < this.selectedAnswers[i].length; j++) { // add border to the answers of current qs
                const ix = this.selectedAnswers[i][j];
                dynamicContainers[i].querySelectorAll('.answer')[ix].classList.add('selected')
            }
        }

        const allAnswered = arraySum(this.selectedAnswers.map(x => x !== null)) === this.nQuestions;
        if (allAnswered && !this.quizFinished) {
            this.submitButton.classList.remove('d-none');
        } else {
            this.submitButton.classList.add('d-none');
        }
    }

    #updateCurrentQuestionAndDisplayNext(event) { // eslint-disable-line require-jsdoc
        // Select the current question
        const currentQuestion = document.querySelector('.question-selected');
        const selectedValues = this.#selectAnswer(currentQuestion);
        const alreadyAnswered = this.selectedAnswers[this.#currentQuestionIndex] !== null;

        this.selectedAnswers[this.#currentQuestionIndex] = null;
        if (selectedValues.length > 0) {
            this.selectedAnswers[this.#currentQuestionIndex] = selectedValues;
        }
        localStorage.setItem('activeQuiz', JSON.stringify({
            id: this.quizData.id,
            selectedAnswers: this.selectedAnswers
        }))

        const soFar = arraySum(this.selectedAnswers.map(x => x !== null))
        if (alreadyAnswered) {
            console.log(`[${now()}] Qs: ${this.#currentQuestionIndex}. Updating an existing answer. Total: ${soFar}`)
        } else {
            if (selectedValues.length === 0) {
                console.log(`[${now()}] Qs: ${this.#currentQuestionIndex}. Skipping question. Total: ${soFar}`)
            } else {
                console.log(`[${now()}] Qs: ${this.#currentQuestionIndex}. Adding a new answer. Total: ${soFar}`)
            }
        }

        if (soFar === this.nQuestions) {
            this.#currentQuestionIndex = (this.#currentQuestionIndex + 1) % this.nQuestions;
        } else {
            let i = (this.#currentQuestionIndex + 1) % this.nQuestions;
            while (i !== this.#currentQuestionIndex && this.selectedAnswers[i] !== null) {
                i = (i + 1) % this.nQuestions;
            }
            this.#currentQuestionIndex = i;
        }
        this.#displayCurrentQuestion();
    }

    /**
     * @param {Element} question The question whose answer we are selecting
     * @return An array of indexes for the selected answers.
     */
    #selectAnswer(question) {
        const answer = question.querySelectorAll('.answer');
        const res = [];
        for (let i = 0; i < answer.length; i++) {
            if (answer[i].classList.contains('selected')) {
                res.push(i);
            }
        }
        return res;
    }

    /**
     * @param {Event} event On click even when selecting an answer
     */
    #handleAnswerSelection(event) {
        const answer = event.target;
        answer.classList.toggle('selected');
    }

    #setupQuestionSlots() { // eslint-disable-line require-jsdoc
        const questionSlots = document.querySelectorAll('.question-slot');
        questionSlots.forEach(slot => {
            slot.addEventListener('click', (event) => this.#setupQuestionSlot(event, slot))
        });
    }

    /**
     * @param {Event} event The 'click' event that triggered this hanlder
     * @param {Element} slot The slot element from the bottom of the screen.
     */
    #setupQuestionSlot(event, slot) {
        // Get question index
        const questionIndex = slot.textContent.replace('Q', '');
        // Set the currentQuestionIndex to the index of the clicked question
        this.#currentQuestionIndex = questionIndex - 1;
        // Display the clicked question
        this.#displayCurrentQuestion();
    }

    /**
     * @brief submits the currently answered questions to the backend for evaluation and returns the score and responses
     */
    #submitQuiz(event) {
        this.submitButton.disabled = true;
        this.forceFinishButton.disabled = true;
        const errorMsg = document.getElementById('error-msg');
        const soFar = arraySum(this.selectedAnswers.map(x => x !== null))
        const sessionToken = localStorage.getItem('sessionToken');
        console.log(`[${now()}] Submitting ${soFar} answers`)

        fetch('evaluate', {
            method: 'POST',
            headers: {
                Accept: 'application/json, text/plain, */*',
                'Content-Type': 'application/json',
                'X-DRPCIVFlask-SessionToken': sessionToken
            },
            body: JSON.stringify({ id: this.quizData.id, questionsResponses: this.selectedAnswers })
        }).then((response) => {
            return new Promise((resolve) => response.json()
                .then((json) => resolve({
                    status: response.status,
                    json
                })));
        }).then(({ status, json }) => {
            switch (status) {
            case 200:
                this.#updatePageWithResult(this, json)
                break;
            default:
                console.log(`[${now()}] Status: ${status}. Error: '${JSON.stringify(json)}'.`)
                errorMsg.innerHTML = JSON.stringify(json)
                break
            }
        })
            .catch(error => {
                console.error(error);
            });
    }

    /**
     * @brief Provides a final page after the user submits with the backend result
     * @property {QuizManager} quiz The quiz object
     * @property {JSON} result The json result from the backend
     */
    #updatePageWithResult(quiz, result) {
        const score = result.score
        const correctAnswers = result.correctAnswers
        const correctAnswersList = result.correctAnswersList
        const dynamicContainers = document.querySelectorAll('.dynamic-container');
        const questionSlots = document.querySelectorAll('.question-slot');
        quiz.quizFinished = true;

        quiz.submitButton.classList.add('d-none');
        quiz.nextButton.classList.add('d-none');
        quiz.forceFinishButton.classList.add('d-none');
        // hide all the questions to show the total result first.
        for (let i = 0; i < this.nQuestions; i++) {
            dynamicContainers[i].classList.remove('current')
        }

        const scoreMessage = `
<div id="score-message" class="question"> You got ${score} out of ${this.nQuestions} questions right.
You ${score >= 22 ? 'passed 🎉!' : 'failed 🤡 ... again.'}
</div>`
        const newQuizButton = '<div><button id="start-button" class="green-button">New quiz</button></div>'
        quiz.#quizContainer.insertAdjacentHTML('afterbegin', scoreMessage);
        quiz.#quizContainer.insertAdjacentHTML('beforeend', newQuizButton);

        document.getElementById('start-button').addEventListener('click', startButtonOnClick);
        for (let i = 0; i < quiz.nQuestions; i++) {
            questionSlots[i].classList.remove('slot-answered')
            questionSlots[i].classList.add(correctAnswers[i] ? 'correct-answer' : 'wrong-answer')
            const answers = dynamicContainers[i].querySelectorAll('.answer')
            const currentCorrectAnswers = correctAnswersList[i]

            // Highlight correct answers
            answers.forEach((answer, index) => {
                if (currentCorrectAnswers.includes(index)) {
                    answer.classList.add('correct-answer')
                }
            })
        }
    }
}
