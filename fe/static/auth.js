import { now } from './utils.js'

/**
 * function to check if the user is logged in or we need to call the login/ endpoint
 * @returns true if token is valid, false otherwise (inlcuding errors)
*/
export async function checkIfTokenIsValid() {
    const user = localStorage.getItem('user');
    const sessionToken = localStorage.getItem('sessionToken')
    if (sessionToken == null) {
        return false;
    }

    const result = await fetch(`check_token_status?user=${user}`, {
        method: 'GET',
        headers: { 'X-DRPCIVFlask-SessionToken': sessionToken }
    }).then((response) => {
        return new Promise((resolve) => response.json()
            .then((json) => resolve({
                status: response.status,
                json
            })));
    }).then(({ status, json }) => {
        switch (status) {
        case 200:
            console.log(`[${now()}] ${JSON.stringify(json)}`)
            return json.valid
        default:
            console.log(`[${now()}] Status: ${status}. Error: '${JSON.stringify(json)}'.`)
            return false
        }
    })
        .catch(error => {
            console.error(error);
            return false
        });
    return result;
}

/**
 * Handles on click on the login/ page for local auth only.
 */
export function loginButtonOnClick() { // eslint-disable-line no-unused-vars
    const nameFromBox = document.getElementById('name-input-box').value;
    const errorMsg = document.getElementById('error-msg');
    errorMsg.innerHTML = '';
    if (nameFromBox.length < 3) {
        errorMsg.innerHTML = 'Name must be at least 3 characters long!';
        return
    }

    fetch('login', {
        method: 'POST',
        headers: { Accept: 'application/json, text/plain, */*', 'Content-Type': 'application/json' },
        body: JSON.stringify({ user: nameFromBox })
    }).then((response) => {
        return new Promise((resolve) => response.json()
            .then((json) => resolve({
                status: response.status,
                json
            })));
    }).then(({ status, json }) => {
        switch (status) {
        case 200:
            doLogin(json.user, json.displayName, json.sessionToken)
            break;
        default:
            console.log(`[${now()}] Status: ${status}. Error: '${JSON.stringify(json)}'.`)
            errorMsg.innerHTML = JSON.stringify(json)
            break
        }
    })
        .catch(error => {
            console.error(error);
        });
}

/**
 * Handles on click on the create_user/ page for local auth only.
 */
export function createUserButtonOnclick() { // eslint-disable-line no-unused-vars
    const nameFromBox = document.getElementById('name-input-box').value;
    const errorMsg = document.getElementById('error-msg');
    errorMsg.innerHTML = '';
    if (nameFromBox.length < 3) {
        errorMsg.innerHTML = 'Name must be at least 3 characters long!';
        return
    }

    fetch('create_user', {
        method: 'PUT',
        headers: { Accept: 'application/json, text/plain, */*', 'Content-Type': 'application/json' },
        body: JSON.stringify({ user: nameFromBox, displayName: nameFromBox })
    }).then((response) => {
        return new Promise((resolve) => response.json()
            .then((json) => resolve({
                status: response.status,
                json
            })));
    }).then(({ status, json }) => {
        console.log(`[${now()}] Status: ${status}. Error: '${JSON.stringify(json)}'.`)
        errorMsg.innerHTML = JSON.stringify(json)
    })
        .catch(error => {
            console.error(error);
        });
}

/**
 * Called from the login page after a successful google auth. Passes the mail/display name and api session token.
 */
export function googleLogin() { // eslint-disable-line no-unused-vars
    const user = document.getElementById('google-user').textContent
    const displayName = document.getElementById('google-displayName').textContent
    const sessionToken = document.getElementById('google-sessionToken').textContent
    doLogin(user, displayName, sessionToken)
}

/**
 * Sets up the local storage with the current login information. Redirects to index.
 * @param {str} user The username for this user used in API calls. For google auth, this is the e-mail.
 * @param {str} displayName The display name used in leaderboard
 * @param {str} sessionToken The session token used to authenticate API calls (get_quiz, evalute, logout etc.)
 */
export function doLogin(user, displayName, sessionToken) {
    localStorage.setItem('sessionToken', sessionToken)
    localStorage.setItem('user', user)
    localStorage.setItem('displayName', displayName)
    console.log(`[${now()}] Received a new token id: '${sessionToken}'.`)
    window.location.href = '/';
}

/**
 * Logs user out.
 */
export function logoutButtonOnClick() { // eslint-disable-line no-unused-vars
    const errorMsg = document.getElementById('error-msg')
    const sessionToken = localStorage.getItem('sessionToken')
    const user = localStorage.getItem('user')
    fetch(`logout?user=${user}`, {
        method: 'GET',
        headers: { 'X-DRPCIVFlask-SessionToken': sessionToken }
    }).then((response) => {
        return new Promise((resolve) => response.json()
            .then((json) => resolve({
                status: response.status,
                json
            })));
    }).then(({ status, json }) => {
        // remove them, regardless
        localStorage.removeItem('sessionToken')
        localStorage.removeItem('user')
        localStorage.removeItem('displayName')
        switch (status) {
        case 200:
            console.log(`[${now()}] ${JSON.stringify(json)}`)
            break;
        default:
            console.log(`[${now()}] Status: ${status}. Error: '${JSON.stringify(json)}'.`)
            errorMsg.innerHTML = JSON.stringify(json)
            break
        }
        window.location.href = '/';
    })
        .catch(error => {
            console.error(error);
        });
}
