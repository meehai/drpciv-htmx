/**
 * Define JSON
 * @typedef {string} JSON
 */

/**
 * Returns the current timestamp in ISO format
 */
export function now() {
    return (new Date()).toISOString().slice(0, -1); // cut the 'Z' from the end
}

/**
 * @param {int} n The number of elements
 * @param {int} value The value to fill
 * @returns {Array} An array filled with the value n times
 */
export function arrayFill(n, value) {
    const res = Array(n);
    for (let i = 0; i < n; i++) {
        res[i] = value;
    }
    return res
}

/**
 * @param {Array} arr An array of numbers
 * @returns {int | float} The sum of the array of numbers
 */
export function arraySum(arr) {
    let sum = 0;
    for (let i = 0; i < arr.length; i++) {
        sum += arr[i];
    }
    return sum;
}
