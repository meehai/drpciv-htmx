import { now } from './utils.js'
import { checkIfTokenIsValid, logoutButtonOnClick } from './auth.js'

let changeNameToggle = false

/**
 * On click handler for the 'change name' button in the profile page.
 */
function submitChangeNameOnClick() {
    const sessionToken = localStorage.getItem('sessionToken');
    const user = localStorage.getItem('user');
    const newDisplayName = document.getElementById('change-name-input').value
    const displayName = localStorage.getItem('displayName');
    const errorMsg = document.getElementById('error-msg');

    if (displayName === newDisplayName) {
        changeNameOnClick()
        return
    }

    fetch('update_display_name', {
        method: 'PUT',
        headers: {
            Accept: 'application/json, text/plain, */*',
            'Content-Type': 'application/json',
            'X-DRPCIVFlask-SessionToken': sessionToken
        },
        body: JSON.stringify({ user, newDisplayName })
    }).then((response) => {
        return new Promise((resolve) => response.json()
            .then((json) => resolve({
                status: response.status,
                json
            })));
    }).then(({ status, json }) => {
        if (status === 200) {
            localStorage.setItem('displayName', newDisplayName)
            changeNameOnClick()
        } else {
            errorMsg.innerHTML = JSON.stringify(json)
        }
        console.log(`[${now()}] Status: ${status}. JSON: '${JSON.stringify(json)}'.`)
    })
        .catch(error => {
            console.error(error);
        });
}

/**
 * On click handler for the 'edit' button. Toggles the <input type=text> with the <a href>
 */
function changeNameOnClick() {
    changeNameToggle = !changeNameToggle;
    const displayName = localStorage.getItem('displayName');
    const displayNameSpan = document.getElementById('display-name-span')
    const displayNameSubmit = document.getElementById('change-name-submit-button')
    const changeNameButton = document.getElementById('change-name-edit-button')
    if (changeNameToggle === true) {
        displayNameSpan.innerHTML = `<input type="text" id="change-name-input" value=${displayName}>`
        displayNameSubmit.classList.remove('d-none')
        changeNameButton.textContent = 'Cancel'
    } else {
        displayNameSpan.innerHTML = `<a href="profile"><b>${displayName}</b></a>`
        displayNameSubmit.classList.add('d-none')
        changeNameButton.textContent = 'Edit'
    }
}

/**
 * function to check if the user is logged so we know what html to server to user (get new quiz or login)
 */
export async function profileDynamicUpdate() { // eslint-disable-line no-unused-vars
    const displayName = localStorage.getItem('displayName');
    const profileContainer = document.getElementById('profile-container')
    const user = localStorage.getItem('user')
    if (user == null || !await checkIfTokenIsValid()) {
        console.log(`[${now()}] Not logged in or invalid token. Redirecting to main page.`)
        window.location.href = '/';
    } else {
        profileContainer.innerHTML = `
<p>
    Hello, <span id="display-name-span"><a href="profile"><b>${displayName}</b></a></span>!
<button id="change-name-edit-button" class="green-button"> Edit </button>
<button id="change-name-submit-button" class="blue-button d-none"> Submit </buton>
<button id="logout-button" class="red-button">Logout</button>
</p>
<div id="error-msg"> </div>`
        document.getElementById('logout-button').addEventListener('click', logoutButtonOnClick);
        document.getElementById('change-name-edit-button').addEventListener('click', changeNameOnClick);
        document.getElementById('change-name-submit-button').addEventListener('click', submitChangeNameOnClick);
    }
}
