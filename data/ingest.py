#!/usr/bin/env python3
"""
ingest data for DRPCIV
Install these deps too: beautifulsoup4>=4.12.3 & tqdm>=4.64.1
"""
from argparse import ArgumentParser, Namespace
from pathlib import Path
import base64
from datetime import datetime
from loggez import loggez_logger as logger
from bs4 import BeautifulSoup # pylint: disable=import-error
from tqdm import trange, tqdm # pylint: disable=import-error
import requests

import sys # pylint: disable=wrong-import-order
sys.path.append(f"{Path(__file__).parents[1]}/be")
from db_handler import DBHandler, LibSQLDBHandler # pylint: disable=wrong-import-position
from drpciv_db_handler import QuestionAnswers # pylint: disable=wrong-import-position

class ExamenDRPCIV:
    """get data from this endpoint"""
    @staticmethod
    def _endpoint_fmt(question_id: int) -> str:
        """given an endpoint url from the supported ones and a question id, returns the formated endpoint"""
        return f"https://examendrpciv.ro/categoria-B-{question_id}/" # note the leading / at the end.

    def get_qa(self, question_id: int) -> QuestionAnswers:
        """gets one question given an integer id"""
        assert question_id > 0 and isinstance(question_id, int), f"Expected int > 0, got {question_id}"
        r = requests.get(self._endpoint_fmt(question_id), timeout=60)
        soup = BeautifulSoup(r.content, "html.parser")
        question = soup.find("h1", itemprop="name").get_text().strip()
        img = soup.find("img", class_="img-fluid")
        if img is not None:
            img_raw_data = requests.get(f"https://examendrpciv.ro/{img.get('src')}", timeout=60).content
            img = base64.b64encode(img_raw_data)
        answers_html = soup.find_all("div", itemprop="suggestedAnswer")
        answers = [a.find("div", class_="card-header").get_text().split("\n")[2].strip() for a in answers_html]
        correct_answers = [ix for ix in range(len(answers_html)) if "bg-success" in answers_html[ix].attrs["class"]]
        return QuestionAnswers(question_id, question, answers, correct_answers, img)

def insert_qa(db_handler: DBHandler, data: QuestionAnswers):
    """inserts a QuestionAnswers object to the DB. Images are optional. Answers are added as strings"""
    answers = "|".join(data.answers)
    if data.image is None:
        query = "insert into qa(id, question, answers, correct_answers) values(?, ?, ?, ?)"
        parameters = (data.qid, data.question, answers, ",".join(map(str, data.correct_answers)))
    else:
        query = "insert into qa(id, question, answers, correct_answers, image) values(?, ?, ?, ?, ?)"
        parameters = (data.qid, data.question, answers, ",".join(map(str, data.correct_answers)), data.image)
    db_handler.query(query, parameters, blocking=True)

def get_args() -> Namespace:
    """cli args"""
    parser = ArgumentParser()
    parser.add_argument("--output_path", "-o", type=str, required=True)
    parser.add_argument("--force", action="store_true")
    parser.add_argument("--endpoint", default="examendrpciv.ro", choices=["examendrpciv.ro"])
    parser.add_argument("--insert_strategy", choices=["during_download", "after_download"], default="after_download")
    parser.add_argument("--token_path")
    args = parser.parse_args()
    if args.force:
        assert args.output_path.find("libsql") == 1
        logger.info(f"--force provided. Removing '{args.output_path}' first.")
        Path(args.output_path).unlink()
    return args

def main(args: Namespace):
    """main fn"""
    crawler = {"examendrpciv.ro": ExamenDRPCIV()}[args.endpoint]
    total = 1191 # TODO find a way to get this number better.
    db = LibSQLDBHandler(args.output_path, token_path=args.token_path)
    existing_ix = [x[0] for x in db.query("select id from qa").fetchall()]
    logger.info(f"Existing: {len(existing_ix)}")

    res = []
    for i in trange(1, total + 1, desc="Downloading"):
        if i in existing_ix:
            logger.info(f"Question {i} exists. Skipping.")
            continue
        try:
            res.append(item := crawler.get_qa(i))
            if args.insert_strategy == "during_download":
                insert_qa(db, item)
        except Exception as e:
            logger.debug(f"error at question {i}. skipping.")
            open("exceptions.txt", "a").write(f"[{datetime.now().isoformat()}. Q: {i}] {e}.\n")
    if args.insert_strategy == "after_download":
        for item in tqdm(res, desc="inserting"):
            insert_qa(db, item)

if __name__ == "__main__":
    main(get_args())
