import globals from 'globals'

import path from 'path'
import { fileURLToPath } from 'url'
import { FlatCompat } from '@eslint/eslintrc'
import pluginJs from '@eslint/js'
import jsdoc from 'eslint-plugin-jsdoc';

// mimic CommonJS variables -- not needed if using CommonJS
const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)
const compat = new FlatCompat({ baseDirectory: __dirname, recommendedConfig: pluginJs.configs.recommended })

export default [
    {
        files: ['**/*.js'],
        languageOptions: { sourceType: 'script' },
        plugins: {
            jsdoc,
        },
    },
    { languageOptions: { globals: globals.browser } },
    ...compat.extends('standard'),
    ...compat.config({
        rules: {
            'space-before-function-paren': ['off'],
            indent: ['error', 4],
            semi: 0,
            "require-jsdoc": ["error", {
                "require": {
                    "FunctionDeclaration": true,
                    "MethodDefinition": true,
                    "ClassDeclaration": true,
                    "ArrowFunctionExpression": true,
                    "FunctionExpression": true
                }
            }],
        }
    })
]
