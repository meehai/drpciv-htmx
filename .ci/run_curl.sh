#!/usr/bin/bash

function dump_logs {
    echo -e "\n--> Dumping logs from run:"
    cat /tmp/logs.txt
}

set -e
export CWD=$(dirname $(readlink -f ${BASH_SOURCE[0]}))
( kill $(netstat -tlpn | grep 42070 | tr -s "  " " " | cut -d " " -f7 | cut -d "/" -f1) || echo "" ) >/dev/null 2>&1
echo "- curl test RUNNING"

git restore $CWD/../data/drpciv.db
echo "-- starting server"
LOGGEZ_LOGLEVEL=-1 python $CWD/../be/app.py --port 42070 --db_path $CWD/../data/drpciv.db 2>/tmp/logs.txt &
PID=$!
test "$?" == "0" || ( echo "--- server error"; kill $$; dump_logs)
sleep 1

export NAME=$(tr -dc A-Za-z0-9 </dev/urandom | head -c 13)
echo "-- random name: $NAME"

echo "-- testing server"
curl localhost:42070  >> /tmp/logs.txt 2>&1 || ( echo "--- server error"; kill $PID; kill $$; dump_logs) \
    && echo "--- OK"

echo "-- creating user"
loginReq=$(curl -X PUT -H "Content-Type: application/json" \
    -d "{\"user\": \"$NAME\", \"displayName\": \"$NAME\"}" \
    localhost:42070/create_user 2>/dev/null | tee -a /tmp/logs.txt)
newUser=$(echo $loginReq | jq .newUser)
test "$newUser" == "true" || ( echo "Expected new user, got: $loginReq"; kill $PID; kill $$; dump_logs) \
    && echo "--- OK"

echo "-- getting new quiz before logging in"
quizData=$(curl localhost:42070/get_quiz?user=$NAME -H "X-DRPCIVFlask-SessionToken: LALALA" 2>/dev/null)
quizErr=$(echo $quizData | jq -r .error)
test "$quizErr" == "Authentication error: User: '$NAME' has no session tokens in DB" \
    || ( echo "Expected auth error, got: $quizErr"; kill $PID; kill $$; dump_logs) && echo "--- OK"

echo "-- logging in"
sessionToken=$(curl -X POST -H "Content-Type: application/json" \
    -d "{\"user\": \"$NAME\"}" \
    localhost:42070/login 2>/dev/null | tee -a /tmp/logs.txt | jq -r ".sessionToken")
test "$sessionToken" != "null" || ( echo "--- session token is null"; kill $PID; kill $$; dump_logs) \
    && echo "--- session token: $sessionToken"

# killing and restarting server
kill $PID
echo "-- restarting server"
LOGGEZ_LOGLEVEL=-1 python $CWD/../be/app.py --port 42070 --db_path $CWD/../data/drpciv.db 2>/tmp/logs.txt &
PID=$!
test "$?" == "0" || ( echo "--- server error"; kill $$; dump_logs)
sleep 1

echo "-- sanity check: should get same token for a secondary login"
sessionToken2=$(curl -X POST -H "Content-Type: application/json" \
    -d "{\"user\": \"$NAME\"}" localhost:42070/login 2>/dev/null \
    | tee -a /tmp/logs.txt | jq -r ".sessionToken")
test "$sessionToken" == "$sessionToken2" \
    || ( echo --- sanity check: "$sessionToken" vs "$sessionToken2"; kill $PID; kill $$; dump_logs) \
    && echo "--- OK"

echo "-- getting new quiz"
quizData=$(curl localhost:42070/get_quiz?user=$NAME -H "X-DRPCIVFlask-SessionToken: $sessionToken" 2>/dev/null)
quizId=$(echo $quizData | jq -r ".id")
test "$quizId" != "null" || ( echo "--- quiz id is null"; kill $PID; kill $$; dump_logs) \
    && echo "--- quiz id: $quizId"

echo "-- sanity check: should get same quiz id for a secondary request"
quizData2=$(curl localhost:42070/get_quiz?user=$NAME -H "X-DRPCIVFlask-SessionToken: $sessionToken" 2>/dev/null)
quizId2=$(echo $quizData2 | jq -r ".id")
test "$quizId" == "$quizId2" \
    || ( echo "--- sanity check: $quizId vs $quizId2"; kill $PID; kill $$; dump_logs) && echo "--- OK"

echo "-- evaluating"
evalData=$(curl -X POST -H "Content-Type: application/json" \
    -d "{\"user\": \"$NAME\", \"id\":\"$quizId\", \
         \"questionsResponses\":[[2],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1], \
                                 [0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1]]}" \
    -H "X-DRPCIVFlask-SessionToken: $sessionToken" \
    localhost:42070/evaluate 2>/dev/null | tee -a /tmp/logs.txt)
score=$(echo $evalData | jq ".score")
echo "--- score: $score"

echo "-- sanity check: should get the same score for a secondary request"
evalData2=$(curl -X POST -H "Content-Type: application/json" \
    -d "{\"user\": \"$NAME\", \"id\":\"$quizId\", \
         \"questionsResponses\":[[2],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1], \
                                 [0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1],[0,1]]}" \
    -H "X-DRPCIVFlask-SessionToken: $sessionToken" \
    localhost:42070/evaluate 2>/dev/null | tee -a /tmp/logs.txt)
score2=$(echo $evalData2 | jq ".score")
test "$score" == "$score2" || ( echo --- sanity check: "$score" vs "$score2"; kill $PID; kill $$; dump_logs) \
    && echo "--- OK"

echo "-- logout"
logoutReq=$(curl localhost:42070/logout?user=$NAME -H "X-DRPCIVFlask-SessionToken: $sessionToken" 2>/dev/null \
    | tee -a /tmp/logs.txt)
logoutData=$(echo $logoutReq | jq .logOut)
test "$logoutData" == "true" || ( echo "Expected logout, got: $logoutReq"; kill $PID; kill $$; dump_logs) \
    && echo "--- OK"

echo "-- sanity check: cannot logout twice"
logoutReq2=$(curl localhost:42070/logout?user=$NAME -H "X-DRPCIVFlask-SessionToken: $sessionToken" 2>/dev/null \
    | tee -a /tmp/logs.txt)
logoutData2=$(echo $logoutReq2 | jq -r .error)
test "$logoutData2" == "Authentication error: Expired token" \
    || ( echo "Expected logout fail, got: $logoutData2"; kill $PID; kill $$; dump_logs) && echo "--- OK"

echo "- curl test PASSED"

# cleanup: kill server and restore DB to its initial state
kill $PID
git restore $CWD/../data/drpciv.db
