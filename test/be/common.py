import sys
from pathlib import Path
from typing import Iterator
import pytest
import random
from flask.testing import FlaskClient

sys.path.append(f"{Path(__file__).parents[2] / 'be'}")
from utils import get_project_root
from app import _setup_app

@pytest.fixture
def client() -> Iterator[FlaskClient]:
    try:
        import libsql
        db_engine = "libsql" if random.random() < 0.5 else "sqlite"
    except ImportError: # libsql is not installed locally
        db_engine = "sqlite"

    app = _setup_app(db_engine=db_engine,
                     db_path=f"{get_project_root()}/data/drpciv.db",
                     fe_path=get_project_root() / "fe",
                     auth_type="local_auth", quiz_duration_s=30 * 60,
                     session_token_duration_s=60 * 60 * 24)
    app.config.update({"TESTING": True})
    with app.test_client() as client:
        yield client

def create_user_get_session_token(client: FlaskClient, name: str) -> str:
    _ = client.put("/create_user", json={"user": name, "displayName": name})
    login_req = client.post("/login", json={"user": name})
    return login_req.json["sessionToken"]
