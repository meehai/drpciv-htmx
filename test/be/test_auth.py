import random
import time
from datetime import datetime, timezone as tz
from flask.testing import FlaskClient
from common import client, create_user_get_session_token

def test_login(client: FlaskClient):
    rand_name = "".join(map(chr, (random.randint(ord('a'), ord('z')) for _ in range(10))))
    _ = client.put("/create_user", json={"user": rand_name, "displayName": f"display_{rand_name}"})

    login_req_bad = client.post("/login", json={"userlala": rand_name})
    assert login_req_bad.status_code == 400, getattr(login_req_bad, "json", "no json")
    assert login_req_bad.json["error"].startswith("expected 'user' key for 'login/' endpoint.")

    login_req = client.post("/login", json={"user": rand_name})
    assert len(login_req.json) == 4, login_req.json.keys()
    assert login_req.json["auth"] == True
    assert login_req.json["displayName"] == f"display_{rand_name}"
    assert login_req.json["user"] == rand_name
    assert len(login_req.json["sessionToken"]) == 10

    headers = {"X-DRPCIVFlask-SessionToken": login_req.json["sessionToken"]}
    bad1 = client.get(f"/check_token_status?user={rand_name}_1", headers=headers) # invalid user (not logged in)
    assert bad1.status_code == 400, getattr(bad1, "json", "no json")
    bad_header1 = {"X-DRPCIVFlask-SessionToken1": login_req.json["sessionToken"]} # bad key
    bad_bh1 = client.get(f"/check_token_status?user={rand_name}", headers=bad_header1)
    assert bad_bh1.status_code == 400, getattr(bad_bh1, "json", "no json")
    bad_header2 = {"X-DRPCIVFlask-SessionToken": login_req.json["sessionToken"] + "1"} # bad value
    bad_bh2 = client.get(f"/check_token_status?user={rand_name}", headers=bad_header2)
    assert bad_bh2.status_code == 400, getattr(bad_bh2, "json", "no json")
    res = client.get(f"/check_token_status?user={rand_name}", headers=headers)
    assert res.status_code == 200, getattr(res, "json", "no json")
    assert res.json["valid"] is True

def test_session_token(client: FlaskClient):
    client.application.config["session_token_duration_s"] = 1

    rand_name = "".join(map(chr, (random.randint(ord('a'), ord('z')) for _ in range(10))))
    session_token = create_user_get_session_token(client, rand_name)
    headers = {"X-DRPCIVFlask-SessionToken": session_token}

    assert client.get(f"/check_token_status?user={rand_name}", headers=headers).json["valid"] is True
    time.sleep(1)
    quiz_response = client.get(f"/get_quiz?user={rand_name}", headers=headers)
    assert quiz_response.json["error"] == "Authentication error: Expired token"

    new_token = client.post("/login", json={"user": rand_name}).json["sessionToken"]
    headers = {"X-DRPCIVFlask-SessionToken": new_token}
    quiz_response = client.get(f"/get_quiz?user={rand_name}", headers=headers)

    # tokens get refreshed for token_duration + quiz_duration on get_test to ensure they work properly
    assert (res1 := client.get(f"/check_token_status?user={rand_name}", headers=headers)).status_code == 200
    diff = datetime.fromisoformat(res1.json["valid_until"]) - datetime.now(tz=tz.utc).replace(tzinfo=None)
    assert diff.seconds > client.application.config["quiz_duration_s"] - 5
