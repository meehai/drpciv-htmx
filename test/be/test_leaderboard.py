import random
from flask.testing import FlaskClient

from common import client, create_user_get_session_token

def _eval(client: FlaskClient, quiz_id: str, answers: list, session_token: str):
    res = client.post("/evaluate", json={"id": quiz_id, "questionsResponses": answers},
                      headers={"X-DRPCIVFlask-SessionToken": session_token})
    assert res.status_code == 200, getattr(res, "json", "no json")
    assert res.json["valid"] is True

def _mistake(answer: list[int]) -> list[int]:
    return [0, 1] if len(answer) == 1 else [0]

def test_leaderboard(client: FlaskClient):
    rand_name = "".join(map(chr, (random.randint(ord('a'), ord('z')) for _ in range(10))))
    session_token1 = create_user_get_session_token(client, rand_name)
    # user 1 quiz 1
    quiz_response = client.get(f"/get_quiz?user={rand_name}", headers={"X-DRPCIVFlask-SessionToken": session_token1})
    answers11 = client.application.db_handler.get_correct_answers_from_quiz_id(quiz_response.json["id"])
    _eval(client, quiz_response.json["id"], answers11, session_token1)

    # user 1 quiz 2
    quiz_response = client.get(f"/get_quiz?user={rand_name}", headers={"X-DRPCIVFlask-SessionToken": session_token1})
    answers12 = client.application.db_handler.get_correct_answers_from_quiz_id(quiz_response.json["id"])
    answers12 = [_mistake(answers12[0]), *answers12[1:]]
    _eval(client, quiz_response.json["id"], answers12, session_token1)

    # user 2 quiz 1
    rand_name2 = "".join(map(chr, (random.randint(ord('a'), ord('z')) for _ in range(10))))
    session_token2 = create_user_get_session_token(client, rand_name2)
    quiz_response = client.get(f"/get_quiz?user={rand_name2}", headers={"X-DRPCIVFlask-SessionToken": session_token2})
    answers21 = client.application.db_handler.get_correct_answers_from_quiz_id(quiz_response.json["id"])
    answers21 = [_mistake(answers21[0]), *answers21[1:5], _mistake(answers21[5]), *answers21[6:]]
    _eval(client, quiz_response.json["id"], answers21, session_token2)

    # user 3 quiz 1
    rand_name3 = "".join(map(chr, (random.randint(ord('a'), ord('z')) for _ in range(10))))
    session_token3 = create_user_get_session_token(client, rand_name3)
    quiz_response = client.get(f"/get_quiz?user={rand_name3}", headers={"X-DRPCIVFlask-SessionToken": session_token3})
    answers31 = client.application.db_handler.get_correct_answers_from_quiz_id(quiz_response.json["id"])
    answers31 = [_mistake(answers31[0]), *answers31[1:5], _mistake(answers31[5]), *answers31[6:25],
                 _mistake(answers31[25])]
    _eval(client, quiz_response.json["id"], answers31, session_token3)

    res_leaderboard = client.post("/leaderboard")
    assert len(res_leaderboard.json) == 3, len(res_leaderboard.json)
    assert res_leaderboard.json[0]["avg"] == 25.5 and res_leaderboard.json[0]["displayName"] == rand_name
    assert res_leaderboard.json[1]["avg"] == 24 and res_leaderboard.json[1]["displayName"] == rand_name2
    assert res_leaderboard.json[2]["avg"] == 23 and res_leaderboard.json[2]["displayName"] == rand_name3
