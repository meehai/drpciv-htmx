import sys
from pathlib import Path
import pytest
sys.path.append(f"{Path(__file__).parents[2] / 'be'}")
from utils import format_list_for_query

def test_format_list_for_query():
    assert format_list_for_query([1, 2, "asdf"]) == "1,2,asdf"
    with pytest.raises(AssertionError):
        format_list_for_query([1, 2.5, "adf"])
        format_list_for_query("Asdf")
