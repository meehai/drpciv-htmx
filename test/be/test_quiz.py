import random
from copy import deepcopy
from flask.testing import FlaskClient

from common import client, create_user_get_session_token

def test_get_quiz(client: FlaskClient):
    rand_name_1 = "".join(map(chr, (random.randint(ord('a'), ord('z')) for _ in range(10))))
    session_token_1 = create_user_get_session_token(client, rand_name_1)
    response_1 = client.get(f"/get_quiz?user={rand_name_1}", headers={"X-DRPCIVFlask-SessionToken": session_token_1})
    assert response_1.status_code == 200, getattr(response_1, "json", "nojson")
    assert "questionsAnswers" in response_1.json

    # If we query the endpoint twice for the same user, we get the same exact test
    response_2 = client.get(f"/get_quiz?user={rand_name_1}", headers={"X-DRPCIVFlask-SessionToken": session_token_1})
    assert response_2.status_code == 200, getattr(response_2, "json", "nojson")
    assert "questionsAnswers" in response_2.json
    assert response_1.json == response_2.json

    # but, the third time we don't (for a new user)
    rand_name_3 = "".join(map(chr, (random.randint(ord('a'), ord('z')) for _ in range(10))))
    session_token_3 = create_user_get_session_token(client, rand_name_3)
    response_3 = client.get(f"/get_quiz?user={rand_name_3}", headers={"X-DRPCIVFlask-SessionToken": session_token_3})
    assert response_3.status_code == 200, getattr(response_3, "json", "nojson")
    assert "questionsAnswers" in response_3.json
    assert response_1.json != response_3.json

def test_evaluate(client: FlaskClient):
    rand_name = "".join(map(chr, (random.randint(ord('a'), ord('z')) for _ in range(10))))
    session_token = create_user_get_session_token(client, rand_name)
    headers = {"X-DRPCIVFlask-SessionToken": session_token}
    quiz_response = client.get(f"/get_quiz?user={rand_name}", headers=headers)
    correct_answers = client.application.db_handler.get_correct_answers_from_quiz_id(quiz_response.json["id"])
    payload = {"id": quiz_response.json["id"], "questionsResponses": deepcopy(correct_answers)}

    res = client.post("/evaluate", json={"questionsResponses": payload["questionsResponses"]}, headers=headers)
    assert res.status_code == 400
    assert res.json["error"] == "`id` property not in the provided request. Cannot evaluate"
    res = client.post("/evaluate", json={"id": payload["id"]}, headers=headers)
    assert res.json["error"] == "`questionsResponses` property not in the provided request. Cannot evaluate"
    assert res.status_code == 400

    res = client.post("/evaluate", json=payload, headers=headers)
    assert res.status_code == 200
    assert all([x for x in res.json["correctAnswers"]]) and res.json["valid"] is True and res.json["score"] == 26
    assert res.json.keys() == {"valid", "correctAnswers", "correctAnswersList", "score"}
    assert [x==y for x, y in zip(payload["questionsResponses"], res.json["correctAnswersList"])] == res.json["correctAnswers"]

    payload["questionsResponses"][0] = [3]
    res = client.post("/evaluate", json=payload, headers=headers)
    assert res.status_code == 400
    assert res.json["error"] == "All answers must be between [0:2] meaning A, B or C."

    payload["questionsResponses"] = payload["questionsResponses"][0:-1]
    res = client.post("/evaluate", json=payload, headers=headers)
    assert res.status_code == 400
    assert res.json["error"] == "Expected 26 answers for the quiz. Got 25"

    new_answers = [[0, 1], *correct_answers[1:]] if len(correct_answers[0]) == 1 else [[0], *correct_answers[1:]]
    payload = {"id": quiz_response.json["id"], "questionsResponses": new_answers}
    res = client.post("/evaluate", json=payload, headers=headers)
    assert res.status_code == 200
    assert [x==y for x, y in zip(payload["questionsResponses"], res.json["correctAnswersList"])] == res.json["correctAnswers"]
    assert all([x for x in res.json["correctAnswers"][1:]]) and res.json["valid"] is True and res.json["score"] == 25
    assert res.json["correctAnswers"][0] is False

if __name__ == "__main__":
    test_evaluate(next(client()))
