import random
import pytest
from flask.testing import FlaskClient
from common import client

def test_create_user(client: FlaskClient):
    rand_name = "".join(map(chr, (random.randint(ord('a'), ord('z')) for _ in range(10))))
    create_1 = client.put("/create_user", json={"user": rand_name, "displayName": rand_name})
    assert create_1.json["newUser"] is True

    create_2 = client.put("/create_user", json={"user": rand_name, "displayName": "lala"})
    assert create_2.json["newUser"] is False

    rand_name_3 = "".join(map(chr, (random.randint(ord('a'), ord('z')) for _ in range(10))))
    create_3 = client.put("/create_user", json={"user": rand_name_3, "displayName": rand_name_3})
    assert create_3.json["newUser"] is True

    create_bad = client.put("/create_user", json={"userlala": rand_name, "displayName": rand_name})
    assert create_bad.status_code == 400
    assert create_bad.json["error"].startswith("Expected 'user' and 'displayName' keys")

def test_change_display_name(client: FlaskClient):
    rand_name = "".join(map(chr, (random.randint(ord('a'), ord('z')) for _ in range(10))))
    create_1 = client.put("/create_user", json={"user": rand_name, "displayName": rand_name})
    assert create_1.json["newUser"] is True

    update_bad_1 = client.put("/update_display_name", json={"lala": 1})
    assert update_bad_1.status_code == 400
    assert update_bad_1.json["error"].startswith("Expected 'user' and 'newDisplayName' keys")

    update_bad_2 = client.put("/update_display_name", json={"user": rand_name, "newDisplayName": "gogu"},
                              headers={"X-DRPCIVFlask-SessionToken": "lala"})
    assert update_bad_2.status_code == 400

    login_req = client.post("/login", json={"user": rand_name})
    update_good = client.put("/update_display_name", json={"user": rand_name, "newDisplayName": "gogu"},
                              headers={"X-DRPCIVFlask-SessionToken": login_req.json["sessionToken"]})
    assert update_good.json == {"newDisplayName": "gogu"}
