import os
import sys
from pathlib import Path
sys.path.append(f"{Path(__file__).parents[1] / 'be'}")
from utils import get_project_root

def pytest_sessionstart(session):
    """
    Called after the Session object has been created and
    before performing collection and entering the run test loop.
    """
    os.system(f"git restore {get_project_root()}/data/drpciv.db")
